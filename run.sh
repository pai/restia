#!/bin/sh

jupyter kernelgateway \
--KernelGatewayApp.api='kernel_gateway.notebook_http' \
--KernelGatewayApp.seed_uri=$(pwd)'/RESTIAP.ipynb'
